db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_ID", ave_stock: {$avg: "$stock"}}}
]);
db.fruits.aggregate([
    {$match : {
        onSale: true,
        origin: 
            {$in: ["Philippines"]}
    }},
    {$group: {
        _id:  "$origin", 
        max: {$max: "$price"}
    }
}
]);
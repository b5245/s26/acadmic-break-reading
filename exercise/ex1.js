let http = require('http');

const port = 4001;

const server = http.createServer((req, res) =>  {
    if(req.url == '/welcome'){
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end('Welcome to the world of Node.js')
    }
    else if(req.url == '/register'){
        res.writeHead(503, {'Content-Type'  : 'text/plain'})
        res.end('Page is under maintenance')
    }
    else{
        res.writeHead(404, {'Content-Type' : 'text/plain'})
        res.end('error')
    }
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);